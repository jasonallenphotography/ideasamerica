class ArticlesController < ApplicationController
  before_action :set_article, only: [:show, :edit, :update, :destroy]

	def index
		@most_recent_articles = Article.paginate(:page => params[:page], :per_page => 10).order('created_at DESC')
	end

	def new
		@article = Article.new
	end


	def create
		@article = Article.new(article_params)
		@article.user = current_user
		if @article.save
			redirect_to '/'
		else
			@errors = @article.errors.full_messages
			render 'new'
		end
	end


	def show
  render layout: 'article'
		
	end


	def edit
	end


	def update
		if @article.update_attributes(article_params)
			redirect_to '/'
		else
			@errors = @article.errors.full_messages
			render 'edit'
		end
	end


	def destroy
		@article.destroy
		redirect_to '/'		
	end


	private
    def set_article
      @article = Article.find(params[:id])
    end

    def article_params
    	params.require(:article).permit(
   				:id,
         	:title,
          :body,
     			:user_id,
     			:published,
      		:img_sm,
        	:img_lg
    		)
    end
end
