class UsersController < ApplicationController
	def show
		@user = User.find(params[:id])
		@articles = @user.articles.paginate(:page => params[:page], :per_page => 10).order('created_at DESC')
		if request.xhr?
			render partial: 'articles', locals: { articles: @articles, user: @user }, layout: false
		end
	end

end
