// This is a manifest file that'll be compiled into application.js, which will include all the files
// listed below.
//
// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, vendor/assets/javascripts,
// or any plugin's vendor/assets/javascripts directory can be referenced here using a relative path.
//
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// compiled file.
//
// Read Sprockets README (https://github.com/rails/sprockets#sprockets-directives) for details
// about supported directives.
//
//= require jquery
//= require jquery_ujs
//= require materialize-sprockets
//= require_tree .

$( document ).ready(function(){
	// Enables collapsible navbar to sidenav
  $(".button-collapse").sideNav();

  // Enables Materialize Parallax effect
  $('.parallax').parallax();
	
  // AJAX for Users ideas
  $('#user-ideas').on('click', '.pagination a', function(e) {
  	e.preventDefault();
  	var $target = $(this);
  	$.ajax({
  		url: $target.attr('href'),
  		method: "GET"
  	}).done( function(response) {
  		$('#user-ideas').html(response)
  	})
  });

  // Social media easing
  $('.social').hover(function() {
      $(this).find('.shutter').stop(true, true).animate({
      bottom: '-36px'
  },
  { duration: 300,
     easing: 'easeOutBounce'
   });
  }, function () {
    $(this).find('.shutter').stop(true, true).animate({
      bottom: 0
   },
   { duration: 300,
     easing: 'easeOutBounce'
    });
    }
  );


  
})