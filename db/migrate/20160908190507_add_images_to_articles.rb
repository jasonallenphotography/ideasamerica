class AddImagesToArticles < ActiveRecord::Migration
  def change
    add_column :articles, :img_sm, :string
    add_column :articles, :img_lg, :string
  end
end
