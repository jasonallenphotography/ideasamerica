User.destroy_all
Article.destroy_all

User.create!(
id: 1,
email: "jasonallen127@gmail.com",
password: "AwesomePassword123!",
reset_password_token: nil,
reset_password_sent_at: nil,
remember_created_at: nil,
sign_in_count: 0,
current_sign_in_at: nil,
last_sign_in_at: nil,
current_sign_in_ip: nil,
last_sign_in_ip: nil,
:created_at => "Mon, 03 Oct 2016 17:00:00 UTC +00:00",
:updated_at => "Mon, 03 Oct 2016 17:00:01 UTC +00:00",
provider: "facebook",
uid: "10101991841845107",
image: "http://graph.facebook.com/v2.6/10101991841845107/picture",
name: "Jason Allen",
bio: "I am a full stack web developer with a mature design aesthetic, a keen eye for UI/UX design, and award winning past creative performance in related fields. I'm able to design and create functional, clean, and beautiful applications and websites that support business goals.\n\n
For the past 9 years, I have listened to what my clients want and translated that into creative solutions that meet their needs. My clients see me as a trusted advisor - they trust that my insight, outside-the-box thinking, and commitment will yield solutions that work both internally for their organization and externally for their customers.",
org: "Freelance Ninja",
website: "http://www.jasoncaryallen.com")

User.create!(            
:id => 2,
:email => "aldobello@mindandmedia.com",
password: "$2a$11$17zaaP5jgqGMVK2slgPLruEPErXmp43GRuNtVdabEtTmOUVEt3WsC",
reset_password_token: nil,
reset_password_sent_at: nil,
remember_created_at: nil,
sign_in_count: 1,
current_sign_in_at: nil,
last_sign_in_at: nil,
current_sign_in_ip: nil,
last_sign_in_ip: nil,
created_at: "Mon, 03 Oct 2016 17:01:00 UTC +00:00",	
updated_at: "Mon, 03 Oct 2016 17:01:01 UTC +00:00",
provider: "facebook",
uid: "10155812193777588",
image: "http://graph.facebook.com/v2.6/10155812193777588/picture",
name: "Aldo Bello",
bio: "As a communications professional, my career started in traditional broadcast production 25 yrs ago. In that time I have seen several transformations take place: the analog to digital shift, the transition from SD to HD, the proliferation of media snacking (consuming small bits of just-in-time content), the progression of interactive online technologies, and the rise of smart mobile. Over the past 5 years, as Chief Creative Officer at Mind & Media, I have been exploring the growing influence of social, interactive and smart mobile in educational, corporate and marketing communications. As broadband penetration in the United States continues to grow and wireless networks become ever more powerful and distributed, interactive and social media solutions to business and organizational problems will also continue to rise in importance and influence. I want Mind & Media to be favorably positioned for this transition.\n\nSpecialties: Strategic communications, adult learning, trans-media campaigns, traditional media, new media, web 2.0, broadband programming, federal sector business knowledge, social networking.",
org: "Mind & Media",
website: "http://www.mindandmedia.com")



Article.create(
	img_lg: "1.jpg",
	created_at: "Mon, 03 Oct 2016 17:00:00 UTC +00:00",
	updated_at: "Mon, 03 Oct 2016 17:00:01 UTC +00:00",
	title: "Hillary Clinton is the only choice",
	body: "By Glorimal Alvarez\n\nMy sister brought me to this country from the Dominican Republic to work at my family’s salon in New York. When my family’s business took off, I was inspired to start my own salon in Virginia. I feel blessed to be doing something I love, and to work everyday alongside my two daughters.
I am supporting Hillary Clinton because I love her. I think that she is a fantastic, strong woman who has the experience to lead this country and that the US is made stronger by people like my family. She recognizes that this country shines when immigrants are given the chance to succeed and contribute. I’m with Hillary because we are stronger together.\n\n
For people like me, this election is too important to sit out. I am very excited about this year’s election, and have been sharing my passion with my clients. I brought my son to register to vote recently. I was so proud that he will be voting in his first election that I told everyone. Now, I ask all my clients if they’re registered and give them information on how to register. I have also been reaching out to youth in my community and encouraging them to register.\n\n
I want to make sure my community turns out in this election. Stand with me: Register to vote at iwillvote.com\n\n
Como una inmigrante y empresaria, Hillary Clinton es la única opción\n\n
Me siento orgullosa de votar por Hillary en Noviembre\n\n
Mi hermana me trajo a este país de la República Dominicana para trabajar en el salón de belleza de mi familia en Nueva York. El negocio tuvo éxito y me motivó a iniciar mi propio salón en Virginia. Me siento feliz haciendo lo que amo junto con mis dos hijas.\n\n
Estoy apoyando a Hillary Clinton porque la amo. Es una mujer ejemplar y fuerte que tiene todas las cualidades para gobernar y cambiar al país. Ella reconoce el rol importante que los inmigrantes tenemos en este país y que los Estados Unidos es más fuerte cuando nosotros los inmigrantes tenemos la oportunidad de sobresalir y contribuir a ese éxito. Estoy con Hillary, porque juntos se puede.\n\n
Para personas como yo, esta elección es demasiada importante para quedarse en casa. Estoy muy emocionada por esta elección y he compartido mi pasión con mis clientes. Recientemente llevé mi hijo para que se registrara a votar y estoy muy orgullosa que votará por primera vez. Se lo he contado a todo el mundo. Ahora le pregunto a cada uno y una de mis clientes si están registrados y registradas y les doy información. También he contactado a los y las jóvenes en mi comunidad para animarlos a votar.\n\n
Quiero asegurarme que mi comunidad vote en esta elección. Párense conmigo: Inscríbete hoy para votar en voyavotar2016.com",
	user_id: 2,
	published: true
)

Article.create(
	img_lg: nil,
	title: "Voto Latino's Hispanic Heritage Month of Action",
	body: %Q( <p><a href="http://www.hhmaction.com/" target="_blank">Hispanic Heritage Month of Action</a> is a one of a kind on-the-ground and digital voter registration campaign that seeks to shift this cultural celebration to a month of action around voter registration and community organizing.&nbsp;It occurs during Hispanic Heritage Month, falling annually between September 15th and October 15th. To learn more, <a href="http://www.hhmaction.com/" target="_blank">click here</a>.</p>),
	user_id: 2,
	published: true
)

Article.create(
	img_lg: "3.jpg",
	title: "VIDEO: Dreams of our Families: Denia Cayetano",
	body: %Q(<p>Content courtesy <a href="https://www.youtube.com/channel/UCdrt3KTczViSTmVnhdCilLQ">United We Dream</a></p><br><iframe width="640" height="360" src="https://www.youtube.com/embed/SMj-bYQF2tk" frameborder="0" allowfullscreen></iframe>),
	user_id: 2,
	published: true
)

Article.create(
	img_lg: "3.jpg",
	title: "VIDEO: Julieta Garibay",
	body: %Q(<p>Content courtesy <a href="https://www.youtube.com/channel/UCdrt3KTczViSTmVnhdCilLQ">United We Dream</a></p><br><iframe width="640" height="360" src="https://www.youtube.com/embed/yQXgeyLBfPo" frameborder="0" allowfullscreen></iframe>),
	user_id: 2,
	published: true
)

Article.create(
  img_lg: "4.jpg",
  title: "VIDEO: 2016 Immigration Filmfest - El Tiempo Latino",
  body: %Q(<p>Content courtesy <a href="http://eltiempolatino.com">El Tiempo Latino</a></p><br><iframe width="640" height="360" src="https://www.youtube.com/embed/fmJ4dXS5HqY?rel=0" frameborder="0" allowfullscreen></iframe>),
	user_id: 2,
	published: true
)

Article.create(
  img_lg: nil,
  title: "VIDEO: CHCI & The Latino Vote - El Tiempo Latino",
  body: %Q(<p>Content courtesy <a href="http://eltiempolatino.com">El Tiempo Latino</a></p><br><iframe width="640" height="360" src="https://www.youtube.com/embed/q3aEYZEv3hM?rel=0" frameborder="0" allowfullscreen></iframe>),
	user_id: 2,
	published: true
)

Article.create(
  img_lg: nil,
  title: "VIDEO: Hillary Profile - Ep40",
  body: %Q(<p>Content courtesy <a href="http://eltiempolatino.com">El Tiempo Latino</a></p><br><iframe width="640" height="360" src="https://www.youtube.com/embed/6e06dFKArlo?rel=0" frameborder="0" allowfullscreen></iframe>),
	user_id: 2,
	published: true
)

Article.create(
	img_lg: "2.gif",
	title: "Las humanidades de José Andrés",
	body: %Q(
<p>Lleva una vida reinventándose e impactando su territorio profesional, el del sabor y la cocina, que para él es también la patria de las emociones, la cultura y la solidaridad humana. Ahora, el chef José Andrés acaba de recibir la Medalla a las Humanidades que concede Estados Unidos a quien, como él, ha hecho la diferencia en la manera en que sentimos los sabores y ha enriquecido el tejido cultural estadounidense.</p>
<p>“Yo soy solamente un cocinero”, repite Andrés a quien le pregunte. </p>
<p>Pero un cocinero que se involucra cultural y cívicamente en su comunidad, en sus patrias políticas y emocionales —España y Estados Unidos— y que parece obsesivamente empeñado en cambiar el mundo a través de lo que sabe hacer: cocinar. </p>
<p>Dice que se gana la vida cocinando, que le importa la comida y que quiere contribuir a aliviar el hambre en el mundo. Por eso, cuando el huracán Katrina devastó Nueva Orleans sintió “un llamdo” y cuando un terremoto asoló Haití en 2010 decidió “viajar allí y cocinar para la gente”. Aquel fue, dice, el momento de actuar; pero José Andrés quería ofrecerle a esas comunidades sustentabilidad. Y creó World Central Kitchen.</p>
<p>“Creo que se puede erradicar el hambre, pero necesitamos ser inteligentes e innovadores, y necesitamos hacerlo ahora”, expresa siempre con cierto sentido de urgencia. Porque José Andrés parece que siempre tiene prisa, y seguirle el ritmo en la generación de ideas para otro restaurante o para otra iniciativa social, no es tarea fácil. Y ahora, además, en plena vorágine electoral su nombre también está sobre la mesa. </p>
<p>Durante la recepción en la Casa Blanca que siguió a la entrega de manos del presidente Barack Obama de las medallas a las Artes y Humanidades, el 22 de sptiembre, al chef le preguntaron por su litigio judicial con el candidato presidencial Donald Trump. </p>
<p>“La mejor manera de combatir a cualquier persona que tenga mensajes negativos es tener una buena sonrisa, salir a votar y apostar por un mundo de inclusión y no de exclusión”, dijo Andrés quien retiró su nombre del restaurante del nuevo hotel de Trump en Washington luego de los insultos contra los inmigrantes hispanos con que el candidato republicano inició su campaña como aspirante presidencial.</p>
<p>En mayo pasado, también en Washington, durante un evento de Latino Victory José Andrés declaró que había dejado de ser “independiente” y se había registrado como demócrata ante el enrarecido ambiente político que atraviesa el país exacerbado por el fenómeno Trump. </p>
<p>“Los indocumentados no son el problema, son parte de la solución para hacer Estados Unidos grande otra vez”, defendió el chef español en referencia al eslogan electoral de Trump. </p>
<p>Asegura que no es plato de su gusto esto del menú político, pero Andrés es un hombre de acción y de transformación. Y quedarse callado no es un condimento que utilice en sus recetas. Lo cierto es que a este inmigrante español y ahora hispanounidense, Estados Unidos lo ha acogido con los brazos abiertos casi de manera multiplataforma: “celebrity” que se codea con Hollywood tanto como con los medios de comunicación, conductor de programas de TV con la comida y el sabor español de protagonistas, activista social y comunitario y, además, consejero en temas de nutrición para la Casa Blanca donde ha colaborado con la primera dama Michelle Obama. ¿Se puede pedir más de un inmigrante? Tal vez sí: fue nombrado miembro del Consejo Asesor de Turismo y Viajes del Gobierno de Estados Unidos.</p>
),
	user_id: 2,
	published: true
)

Article.create(
  img_lg: nil,
  title: "VIDEO: Campaign Quickie 14 (First Debate)",
  body: %Q(<p>Content courtesy <a href="http://eltiempolatino.com">El Tiempo Latino</a></p><br><iframe width="640" height="360" src="https://www.youtube.com/embed/3trvtkTk5Os?rel=0" frameborder="0" allowfullscreen></iframe>),
	user_id: 2,
	published: true
)

Article.create(
  img_lg: nil,
  title: "VIDEO: Arturo Vargas at CHCI, Latino Vote - El Tiempo Latino",
  body: %Q(<p>Content courtesy <a href="http://eltiempolatino.com">El Tiempo Latino</a></p><br><iframe width="640" height="360" src="https://www.youtube.com/embed/ptQ2baVJKiw?rel=0" frameborder="0" allowfullscreen></iframe>),
	user_id: 2,
	published: true
)
# #Article Template
# Article.create(
#   img_lg: "",
#   title: "",
#   body: %Q(),
# 	user_id: 2,
# 	published: true
# )
